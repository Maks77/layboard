import $ from "jquery";
import "../libs/slick/slick.min.js";

$(document).ready(function () {
	
	$('.js-top-menu-btn').on('mouseup', function () {
		$('.js-top-menu').toggleClass('is-open')
	})

	$('.add-info-topline__item').on('mouseup', function () {
		$('.add-info-topline__item').removeClass('is-active');
		$(this).addClass('is-active');

		//... код переключения закладок ...
	})


	//input:file
	$('.uploadbtn').on('change', function () {
		console.dir(this)
		let wrapper = $(this).closest('.file-upload-wrapper')

		let infoLine = wrapper.find('.file-upload-path-line');
		let closeBtn = wrapper.find('.file-upload-path-line').find('.upload-close-btn');
		let filename = this.value.slice(12, this.value.length)

		infoLine.css('display', 'flex');
		$('.chosen-file').val(filename);
	})

	$('.upload-close-btn').on('mouseup', function () {
		let wrapper = $(this).closest('.file-upload-wrapper');
		let infoLine = wrapper.find('.file-upload-path-line');
		let fileInput = wrapper.find('.uploadbtn');
		fileInput[0].value = null;
		infoLine.css('display', 'none');
	})


	



	$('.js-drop-profile').on('mouseup', function (e) {
		e.stopPropagation()
		if ($(this).is(e.target) && !$('.profile-drop').is(e.target)) {
			$(this).toggleClass('is-open');
		}
	})

	$(document).on('click', function (e) {
		let dropProfileMenu = $('.js-drop-profile');

		if (!dropProfileMenu.is(e.target)
			&& dropProfileMenu.has(e.target).length === 0) { 
			dropProfileMenu.removeClass('is-open');
		}

	})


	$('.slider').slick({
		slidesToScroll: 1,
		slidesToShow: 3,
		infinite: true,
		dots: true,
		customPaging : function(slider, i) {
			let thumb = $(slider.$slides[i]).data('thumb');
			return '<div class="dot"></div>';
		},
		prevArrow: '<button class="slider-prev slider-btn"><i class="fas fa-arrow-left"></i></button>',
		nextArrow: '<button class="slider-next slider-btn"><i class="fas fa-arrow-right"></i></button>',
		responsive: [
			{
				breakpoint: 1920,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 1600,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: false
				}
			}

		  ]
	})

	$('.slider-news').slick({
		slidesToScroll: 1,
		slidesToShow: 3,
		infinite: true,
		dots: false,
		customPaging : function(slider, i) {
			let thumb = $(slider.$slides[i]).data('thumb');
			return '<div class="dot"></div>';
		},
		prevArrow: '<button class="slider-prev slider-btn"><i class="fas fa-arrow-left"></i></button>',
		nextArrow: '<button class="slider-next slider-btn"><i class="fas fa-arrow-right"></i></button>',
		responsive: [
			{
				breakpoint: 1920,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1,
					infinite: true,
				}
			},
			{
				breakpoint: 1600,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			}

		  ]
	})
})

